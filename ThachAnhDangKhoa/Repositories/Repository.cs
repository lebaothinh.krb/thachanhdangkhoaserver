﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ThachAnhDangKhoa.Interfaces;
using ThachAnhDangKhoa.Models;

namespace ThachAnhDangKhoa.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly ThachAnhDangKhoaContext _context;
        protected readonly DbSet<TEntity> _entities;

        public Repository(ThachAnhDangKhoaContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _entities.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _entities.AddRange(entities);
        }

        public int Count()
        {
            return _entities.Count();
        }

        public TEntity Get(int id)
        {
            return _entities.Find(id);
        }

        public IEnumerable<TEntity> GetAll(List<string> includeString = null)
        {
            if (includeString == null) return _entities;

            IQueryable<TEntity> result = _entities.Include(includeString[0]);
            for (var i = 1; i< includeString.Count; i++)
            {
                result = result.Include(includeString[i]);
            }
            return result;
        }

        public IEnumerable<TEntity> GetByCondition(Expression<Func<TEntity, bool>> predicate, List<string> includeString  = null)
        {
            if (includeString == null) return _entities.Where(predicate);

            IQueryable<TEntity> result = _entities.Include(includeString[0]);
            for (var i = 1; i < includeString.Count; i++)
            {
                result = result.Include(includeString[i]);
            }
            return result.Where(predicate);
        }

        public void Remove(TEntity entity)
        {
            _entities.Remove(entity);
        }

        public void Remove(int id)
        {
            _entities.Remove(_entities.Find(id));
        }

        public void Update(TEntity entity)
        {
            _entities.Update(entity);
        }
    }
}
