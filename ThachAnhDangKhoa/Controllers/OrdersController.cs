﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Newtonsoft.Json;
using ThachAnhDangKhoa.Data;
using ThachAnhDangKhoa.Models;
using ThachAnhDangKhoa.Repositories;
using ThachAnhDangKhoa.ViewModels;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IConfiguration _config;
        private IHostingEnvironment _env;
        private readonly ThachAnhDangKhoaContext _context;
        private Repository<Order> orderRepository;

        public OrdersController(ThachAnhDangKhoaContext context, IHostingEnvironment env, IConfiguration config)
        {
            _context = context;
            _config = config;
            _env = env;
            orderRepository = new Repository<Order>(context);
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("getOrdersByAdmin")]
        public IEnumerable<Order> getOrdersByAdmin()
        {
            var a = orderRepository.GetAll(new List<string>{ "Products", "User"});
            return a;
        }
        
        [HttpGet, Authorize(Roles = "Admin")]
        [Route("deleteOrderByAdmin/{id}")]
        public IActionResult deleteOrderByAdmin([FromRoute] int id)
        {
            orderRepository.Remove(id);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("updateOrderByAdmin"), Authorize(Roles = "Admin")]
        public IActionResult updateOrderByAdmin([FromBody] Order order)
        {
            try
            {
                var currentOrder = orderRepository.Get(order.OrderId);

                var selectedUser = _context.User.Where(u => u.UserId == order.User.UserId).FirstOrDefault();

                if (selectedUser != null)
                {
                    selectedUser.UserName = order.User.UserName;
                    selectedUser.Email = order.User.Email;
                    selectedUser.Address = order.User.Address;
                    selectedUser.PhoneNumber = order.User.PhoneNumber;
                    currentOrder.User = selectedUser;
                }

                var selectProducts = _context.Product.Where(p => p.Order.OrderId == order.OrderId).ToList();

                var newProducts = _context.Product.Where(p => order.Products.Any(t => t.Id == p.Id)).ToList();
                currentOrder.Products = newProducts;
                currentOrder.Status = order.Status;
                _context.SaveChanges();

                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e);
            }

        }

        [HttpGet("updateOrderStatusByAdmin/{id}/{number}"), Authorize(Roles = "Admin")]
        public IActionResult updateOrderStatusByAdmin([FromRoute] int id, int number)
        {
            var selectedOrder = orderRepository.Get(id);
            selectedOrder.Status = (OrderStatus)number;
            orderRepository.Update(selectedOrder);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("onOrder")]
        public IActionResult onOrder([FromBody] OrderUIViewModel order)
        {
            var selectedUser = _context.User.Where(u => u.PhoneNumber == order.phoneNumber).FirstOrDefault();
            if (selectedUser == null)
            {
                selectedUser = new User() { UserName = order.customerName, Email = order.email, Address = order.address, PhoneNumber = order.phoneNumber };
                _context.User.Add(selectedUser);
                _context.SaveChanges();
            }
            else
            {
                selectedUser.Email = order.email;
                selectedUser.UserName = order.customerName;
                selectedUser.Address = order.address;
            }
            var products = new List<Product>();
            for (var i = 0; i < order.products.Count(); i++)
            {
                var selectedProduct =  _context.Product.Where(p => p.Id == order.products[i].Id).FirstOrDefault();
                if (selectedProduct != null) products.Add(selectedProduct);
            }

            var newOrder = new Order() { OrderDate = DateTime.Now, Products = products, Status = OrderStatus.NOTSEEN, User = selectedUser };
            orderRepository.Add(newOrder);
            _context.SaveChanges();

            SendEmail sendEmail = new SendEmail();
            var webRoot = _env.WebRootPath;
            var pathToFile = _env.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "Email"
                            + Path.DirectorySeparatorChar.ToString()
                            + "onOrder.html";
            var subject = _config.GetValue<string>("OrderMessage:Subject");
            var builder = new BodyBuilder();
            using (StreamReader SourceReader = System.IO.File.OpenText(pathToFile))
            {
                builder.HtmlBody = SourceReader.ReadToEnd();
            }
            string messageBody = string.Format(builder.HtmlBody,
                HttpContext.Request.Scheme+":\\"+ HttpContext.Request.Host + Path.DirectorySeparatorChar.ToString() + "Email" + Path.DirectorySeparatorChar.ToString() + "customLogo.jpg",
                selectedUser.UserName,
                order.htmlImgs,
                "#",
                HttpContext.Request.Scheme + ":\\" +  HttpContext.Request.Host + Path.DirectorySeparatorChar.ToString() + "Email" + Path.DirectorySeparatorChar.ToString() + "facebook.svg",
                HttpContext.Request.Scheme + ":\\" +  HttpContext.Request.Host + Path.DirectorySeparatorChar.ToString() + "Email" + Path.DirectorySeparatorChar.ToString() + "zalo.svg"
                );
            sendEmail.SendEmailAsync(selectedUser.Email, subject, messageBody);
            return Ok();
        }
    }
}