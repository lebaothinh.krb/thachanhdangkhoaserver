﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ThachAnhDangKhoa.Models;
using ThachAnhDangKhoa.Repositories;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewesController : ControllerBase
    {
        private readonly ThachAnhDangKhoaContext _context;
        private Repository<News> newsRepository;

        public NewesController(ThachAnhDangKhoaContext context)
        {
            _context = context;
            newsRepository = new Repository<News>(context);
        }

        [HttpGet]
        [Route("getAllNewes/{index}")]
        public IEnumerable<News> getAllProducts([FromRoute] int index)
        {
            return newsRepository.GetAll().Skip(20 * index).Take(20);
        }

        [HttpGet]
        [Route("getNewes")]
        public IEnumerable<News> getNewes()
        {
            return newsRepository.GetAll();
        }

        [HttpGet]
        [Route("getDetailNews/{id}")]
        public News getDetailNews([FromRoute] int id)
        {
            return newsRepository.Get(id);
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("deleteNewsByAdmin/{id}")]
        public IActionResult deleteNewsByAdmin([FromRoute] int id)
        {
            newsRepository.Remove(id);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("updateNewsByAdmin"), Authorize(Roles = "Admin")]
        public IActionResult updateNewsByAdmin([FromBody] News news)
        {
            newsRepository.Update(news);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("createNewsByAdmin"), Authorize(Roles = "Admin")]
        public IActionResult createNewsByAdmin([FromBody] News news)
        {
            news.Id = null;
            newsRepository.Add(news);
            _context.SaveChanges();
            return Ok();
        }
    }
}