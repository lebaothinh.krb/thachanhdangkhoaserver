﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MimeKit;
using ThachAnhDangKhoa.Data;
using ThachAnhDangKhoa.Models;
using ThachAnhDangKhoa.Repositories;
using ThachAnhDangKhoa.ViewModels;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersByRequestController : ControllerBase
    {
        private readonly IConfiguration _config;
        private IHostingEnvironment _env;
        private readonly ThachAnhDangKhoaContext _context;
        private Repository<OrderByRequest> orderByRequestRepository;

        public OrdersByRequestController(ThachAnhDangKhoaContext context, IConfiguration config, IHostingEnvironment env)
        {
            _env = env;
            _config = config;
            _context = context;
            orderByRequestRepository = new Repository<OrderByRequest>(context);
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("getOrdersByRequestByAdmin")]
        public IEnumerable<OrderByRequest> getOrdersByRequestByAdmin()
        {
            return orderByRequestRepository.GetAll(new List<string>{"User"}).OrderBy(o => o.OrderByRequestId); ;
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("deleteOrderByRequestByAdmin/{id}")]
        public IActionResult deleteOrderByRequestByAdmin([FromRoute] int id)
        {
            orderByRequestRepository.Remove(id);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("updateOrderByRequestByAdmin"), Authorize(Roles = "Admin")]
        public IActionResult updateOrderByRequestByAdmin([FromBody] OrderByRequest order)
        {
            try
            {
                var currentOrder = orderByRequestRepository.Get(order.OrderByRequestId);

                var selectedUser = _context.User.Where(u => u.UserId == order.User.UserId).FirstOrDefault();

                if (selectedUser != null)
                {
                    selectedUser.UserName = order.User.UserName;
                    selectedUser.Email = order.User.Email;
                    selectedUser.Address = order.User.Address;
                    selectedUser.PhoneNumber = order.User.PhoneNumber;
                    currentOrder.User = selectedUser;
                }


                currentOrder.DescribeProduct = order.DescribeProduct;
                currentOrder.Status = order.Status;
                _context.SaveChanges();

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("updateOrderStatusByRequestByAdmin/{id}/{number}"), Authorize(Roles = "Admin")]
        public IActionResult updateOrderStatusByRequestByAdmin([FromRoute] int id, int number)
        {
            var selectedOrder = orderByRequestRepository.Get(id);
            selectedOrder.Status = (OrderStatus)number;
            orderByRequestRepository.Update(selectedOrder);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("onOrderByRequest")]
        public IActionResult onOrderByRequest([FromBody] OrderByRequestUIViewModel order)
        {
            var selectedUser = _context.User.Where(u => u.PhoneNumber == order.phoneNumber).FirstOrDefault();
            if (selectedUser == null)
            {
                _context.User.Add(new User() { UserName = order.customerName, Email = order.email, Address = order.address, PhoneNumber = order.phoneNumber });
                selectedUser = _context.User.Where(u => u.PhoneNumber == order.phoneNumber).FirstOrDefault();
            }
            else
            {
                selectedUser.Email = order.email;
                selectedUser.UserName = order.customerName;
                selectedUser.Address = order.address;
            }

            var newOrder = new OrderByRequest() { OrderDate = DateTime.Now, DescribeProduct = order.describeProduct , Status = OrderStatus.NOTSEEN, User = selectedUser };
            orderByRequestRepository.Add(newOrder);
            _context.SaveChanges();

            SendEmail sendEmail = new SendEmail();
            var webRoot = _env.WebRootPath;
            var pathToFile = _env.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "Email"
                            + Path.DirectorySeparatorChar.ToString()
                            + "onOrderByRequest.html";
            var subject = _config.GetValue<string>("OrderMessage:Subject");
            var builder = new BodyBuilder();
            using (StreamReader SourceReader = System.IO.File.OpenText(pathToFile))
            {
                builder.HtmlBody = SourceReader.ReadToEnd();
            }
            string messageBody = string.Format(builder.HtmlBody,
                HttpContext.Request.Scheme + ":\\" + HttpContext.Request.Host + Path.DirectorySeparatorChar.ToString() + "Email" + Path.DirectorySeparatorChar.ToString() + "customLogo.jpg",
                selectedUser.UserName,
                "#",
                HttpContext.Request.Scheme + ":\\" + HttpContext.Request.Host + Path.DirectorySeparatorChar.ToString() + "Email" + Path.DirectorySeparatorChar.ToString() + "facebook.svg",
                HttpContext.Request.Scheme + ":\\" + HttpContext.Request.Host + Path.DirectorySeparatorChar.ToString() + "Email" + Path.DirectorySeparatorChar.ToString() + "zalo.svg"
                );
            sendEmail.SendEmailAsync(selectedUser.Email, subject, messageBody);
            return Ok();
        }
    }
}