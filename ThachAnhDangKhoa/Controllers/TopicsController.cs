﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ThachAnhDangKhoa.Models;
using ThachAnhDangKhoa.Repositories;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TopicsController : ControllerBase
    {
        private readonly ThachAnhDangKhoaContext _context;
        private Repository<Topic> topicRepository;

        public TopicsController(ThachAnhDangKhoaContext context)
        {
            _context = context;
            topicRepository = new Repository<Topic>(context);
        }

        [HttpGet]
        [Route("getTopics")]
        public IEnumerable<Topic> getTopics()
        {
            return topicRepository.GetAll().OrderBy(t => t.TopicId);
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("deleteTopics")]
        public IActionResult deleteTopics([FromBody] IEnumerable<int> listDelete)
        {
            listDelete.ToList().ForEach(l =>
            {
                topicRepository.Remove(l);
            });
            _context.SaveChanges();
            return Ok();
        }

        [HttpGet("addTopic/{param1}"), Authorize(Roles = "Admin")]
        public int addTopic([FromRoute] string param1)
        {
            topicRepository.Add(new Topic() { TopicName = param1 });
            _context.SaveChanges();
            return topicRepository.GetAll().Where(t => t.TopicName == param1).FirstOrDefault().TopicId;
        }
    }
}