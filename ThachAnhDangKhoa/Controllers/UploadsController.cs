﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ThachAnhDangKhoa.ViewModels;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class UploadsController : ControllerBase
    {
        private readonly IHostingEnvironment hostingEnvironment;
        public UploadsController(IHostingEnvironment environment)
        {
            hostingEnvironment = environment;
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("updateImgProductByAdmin")]
        public async Task<IActionResult> updateImgProductByAdmin([FromForm] IFormFile file,[FromForm] string fileName)
        {
            if (HttpContext.Request.Form.Files.Count > 0)
            {
                var end = HttpContext.Request.Form.Files[0].FileName.Split('.');
                fileName = fileName +"."+ end[end.Length - 1];
                var uploads = Path.Combine(hostingEnvironment.WebRootPath, "Images\\Products");
                var fullPath = Path.Combine(uploads, fileName);
                using (var stream = System.IO.File.Create(fullPath))
                {
                    await HttpContext.Request.Form.Files[0].CopyToAsync(stream);
                }
                return Ok(new {
                    src = Path.Combine(HttpContext.Request.Scheme + ":\\\\", Path.Combine(Path.Combine(HttpContext.Request.Host.Value, "Images\\Products"), fileName))
                });
            }
            return BadRequest();

        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("updateImgNewsByAdmin")]
        public async Task<IActionResult> updateImgNewsByAdmin([FromForm] IFormFile file, [FromForm] string fileName)
        {
            if (HttpContext.Request.Form.Files.Count > 0)
            {
                var end = HttpContext.Request.Form.Files[0].FileName.Split('.');
                fileName = fileName + "." + end[end.Length - 1];
                var uploads = Path.Combine(hostingEnvironment.WebRootPath, "Images\\Newes");
                var fullPath = Path.Combine(uploads, fileName);
                using (var stream = System.IO.File.Create(fullPath))
                {
                    await HttpContext.Request.Form.Files[0].CopyToAsync(stream);
                }
                return Ok(new
                {
                    src = Path.Combine(HttpContext.Request.Scheme + ":\\\\", Path.Combine(Path.Combine(HttpContext.Request.Host.Value, "Images\\Newes"), fileName))
                });
            }
            return BadRequest();

        }

        [HttpPost]
        [Route("updateImgEditor")]
        public async Task<IActionResult> updateImgEditor([FromForm] IFormFile file, [FromForm] string fileName)
        {
            if (HttpContext.Request.Form.Files.Count > 0)
            {
                var end = HttpContext.Request.Form.Files[0].FileName.Split('.');
                fileName = fileName + "." + end[end.Length - 1];
                var uploads = Path.Combine(hostingEnvironment.WebRootPath, "Images\\Editor");
                var fullPath = Path.Combine(uploads, fileName);
                using (var stream = System.IO.File.Create(fullPath))
                {
                    await HttpContext.Request.Form.Files[0].CopyToAsync(stream);
                }
                return Ok(new
                {
                    src = Path.Combine(HttpContext.Request.Scheme+":\\\\", Path.Combine(Path.Combine(HttpContext.Request.Host.Value, "Images\\Editor"), fileName))
                }); ;
            }
            return BadRequest();

        }
    }
}