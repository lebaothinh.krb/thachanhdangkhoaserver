﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ThachAnhDangKhoa.Data;
using ThachAnhDangKhoa.Models;
using ThachAnhDangKhoa.Repositories;
using ThachAnhDangKhoa.ViewModels;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ThachAnhDangKhoaContext _context;
        private Repository<User> usersRepository;

        public UsersController(ThachAnhDangKhoaContext context, IConfiguration config)
        {
            _config = config;
            _context = context;
            usersRepository = new Repository<User>(context);
        }

        [HttpGet]
        [Route("registerEmail/{email}")]
        public IActionResult registerEmail(string email)
        {
            var selectedUser = usersRepository.GetAll().Where(u => u.Email == email).FirstOrDefault();
            if (selectedUser != null)
            {
                return Ok(new
                {
                    Messages = "Bạn đã đăng ký email rồi!"
                });
            }
            usersRepository.Add(new Models.User() { Email = email, Address = "undefine", PhoneNumber = "undefine", UserName = "undefine" });
            return Ok();
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("sendInfoByAdmin")]
        public IActionResult sendInfoByAdmin([FromBody] MailForm mailForm)
        {
            var users = usersRepository.GetAll().ToList();
            SendEmail sendEmail = new SendEmail();
            users.ForEach(u =>
            {
                sendEmail.SendEmailAsync(u.Email, mailForm.subject, mailForm.content);
            });
            return Ok();
        }

        [HttpPost]
        [Route("signin")]
        public IActionResult signin([FromBody] SignInForm signinForm)
        {
            if (signinForm == null)
            {
                return BadRequest("Invalid client request");
            }

            var selectedUser = usersRepository.GetByCondition(u => u.Account == signinForm.username).FirstOrDefault();

            if (selectedUser != null && signinForm.password == selectedUser.Password)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, signinForm.username),
                    new Claim(ClaimTypes.Role, "Admin")
                };

                var tokeOptions = new JwtSecurityToken(
                    issuer: "http://localhost:60699",
                    audience: "http://localhost:60699",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return Ok(new { Token = tokenString });
            }
            else
            {
                return Unauthorized();
            }
        }

    }
}