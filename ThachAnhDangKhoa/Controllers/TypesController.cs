﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ThachAnhDangKhoa.Models;
using ThachAnhDangKhoa.Repositories;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypesController : ControllerBase
    {
        private readonly ThachAnhDangKhoaContext _context;
        private Repository<TypeSearch> topicRepository;

        public TypesController(ThachAnhDangKhoaContext context)
        {
            _context = context;
            topicRepository = new Repository<TypeSearch>(context);
        }

        [HttpGet]
        [Route("getTypes")]
        public IEnumerable<TypeSearch> getTopics()
        {
            return topicRepository.GetAll().OrderBy(t => t.TypeId);
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("deleteTypes")]
        public IActionResult deleteTopics([FromBody] IEnumerable<int> listDelete)
        {
            listDelete.ToList().ForEach(l =>
            {
                topicRepository.Remove(l);
            });
            _context.SaveChanges();
            return Ok();
        }

        [HttpGet("addType/{param1}"), Authorize(Roles = "Admin")]
        public int addTopic([FromRoute] string param1)
        {
            topicRepository.Add(new TypeSearch() { TypeName = param1 });
            _context.SaveChanges();
            return topicRepository.GetAll().Where(t => t.TypeName == param1).FirstOrDefault().TypeId;
        }
    }
}