﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ThachAnhDangKhoa.Models;
using ThachAnhDangKhoa.Repositories;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContentsController : ControllerBase
    {
        private readonly ThachAnhDangKhoaContext _context;
        private Repository<Content> contentRepository;

        public ContentsController(ThachAnhDangKhoaContext context)
        {
            _context = context;
            contentRepository = new Repository<Content>(context);
        }

        [HttpGet]
        [Route("getHomeContents")]
        public IEnumerable<Content> getHomeContents()
        {
            return contentRepository.GetAll(new List<string> { "Topic" }).OrderBy(c => c.Id);
                
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("deleteHomeContentByAdmin/{id}")]
        public IActionResult deleteHomeContentByAdmin([FromRoute] int id)
        {
            try {
                contentRepository.Remove(id);
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost("updateHomeContentByAdmin"), Authorize(Roles = "Admin")]
        public IActionResult updateHomeContentByAdmin([FromBody] Content content)
        {
            var selectedTopic = _context.Topic.Where(t => t.TopicId == content.Topic.TopicId).FirstOrDefault();
            if (selectedTopic != null)
            {
                content.Topic = selectedTopic;
                contentRepository.Update(content);
                _context.SaveChanges();
                return Ok();
            }
            return BadRequest(new { messages = "Không Thể Tìm Thấy Chủ Đề Của Nội Dung!" });
        }

        [HttpPost("createHomeContentByAdmin"), Authorize(Roles = "Admin")]
        public IActionResult createHomeContentByAdmin([FromBody] Content content)
        {
            content.Id = null;
            var selectedTopic = _context.Topic.Where(t => t.TopicId == content.Topic.TopicId).FirstOrDefault();
            if (selectedTopic != null)
            {
                content.Topic = selectedTopic;
                contentRepository.Add(content);
                _context.SaveChanges();
                return Ok(content.Id);
            }
            return BadRequest(new { messages = "Không Thể Tìm Thấy Chủ Đề Của Nội Dung!" });
        }
    }
}