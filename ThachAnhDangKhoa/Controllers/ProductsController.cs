﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ThachAnhDangKhoa.Models;
using ThachAnhDangKhoa.Repositories;
using ThachAnhDangKhoa.ViewModels;

namespace ThachAnhDangKhoa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ThachAnhDangKhoaContext _context;
        private Repository<Product> productRepository;
        
        public ProductsController(ThachAnhDangKhoaContext context)
        {
            _context = context;
            productRepository = new Repository<Product>(context);
        }

        [HttpGet]
        [Route("getAllProducts/{index}")]
        public IEnumerable<Product> getAllProducts ([FromRoute] int index)
        {
            return productRepository.GetAll().Skip(20 * index).Take(20);
        }

        [HttpPost]
        [Route("getProducts")]
        public IEnumerable<Product> getProducts([FromBody] SearchingData search)
        {
            var typeAll = _context.Type.Where(t => t.TypeName.ToLower() == "tất cả").FirstOrDefault();
            var topicAll = _context.Topic.Where(t => t.TopicName.ToLower() == "tất cả").FirstOrDefault();
            IEnumerable<Product> products = null;
                if (search.searchingKey == "" && search.topic == topicAll.TopicId && search.type == typeAll.TypeId)
            {
                products = productRepository.GetAll(new List<string> { "ProductTopics", "Type" }).OrderBy(p => p.Id);
            }
            else
            {
                if (search.searchingKey == "")
                {
                    products = productRepository.GetByCondition(p => ((search.topic == topicAll.TopicId || (p.ProductTopics.Any(pt => pt.TopicId == search.topic))) && (p.Type.TypeId == search.type || search.type == typeAll.TypeId)), new List<string> { "ProductTopics", "Type" }).OrderBy(p => p.Id);
                }
                else
                {
                    products = productRepository.GetByCondition(p => ((search.topic == topicAll.TopicId || (p.ProductTopics.Any(pt => pt.TopicId == search.topic))) && (p.Type.TypeId == search.type || search.type == typeAll.TypeId) && p.ProductName.ToLower().Contains(search.searchingKey.ToLower())), new List<string> { "ProductTopics", "Type" }).OrderBy(p => p.Id);
                }

            }
            if (products.ToList().Count > 0)
            {
                products.ToList<Product>().ForEach(p =>
                {
                    p.ProductTopics.ToList().ForEach(pt =>
                    {
                        pt.Topic = _context.Topic.Where(ttp => ttp.TopicId == pt.TopicId).FirstOrDefault();
                        pt.Topic.ProductTopics = null;
                    });
                });
            }
            return products;
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("deleteProductByAdmin/{id}")]
        public IActionResult deleteProductByAdmin([FromRoute] int id)
        {
            productRepository.Remove(id);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("updateProductByAdmin"), Authorize(Roles = "Admin")]
        public IActionResult updateProductByAdmin([FromBody] Product product)
        {
            _context.ProductTopic.RemoveRange(_context.ProductTopic.Where(pt => pt.ProductId == product.Id));
            //var selectedProduct = productRepository.Get(product.Id);
            var selectedProductTopics = new List<ProductTopic>();

            product.ProductTopics.ToList().ForEach(pt =>
            {
                selectedProductTopics.Add(new ProductTopic() { Product = product, Topic = _context.Topic.Where(t => t.TopicId == pt.TopicId).FirstOrDefault() });
            });

            product.ProductTopics = selectedProductTopics;
            //selectedProduct.ImgsToList = product.ImgsToList;
            //selectedProduct.Material = product.Material;
            productRepository.Update(product);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("createProductByAdmin"), Authorize(Roles = "Admin")]
        public IActionResult createProductByAdmin([FromBody] Product product)
        {
            product.Id = null; 
            product.ProductTopics.ToList().ForEach(pt =>
            {
                pt.Topic = null;
            });
            product.Type = _context.Type.Where(t => t.TypeId == product.Type.TypeId).FirstOrDefault();
            productRepository.Add(product);
            _context.SaveChanges();
            return Ok();
        }
    }
}