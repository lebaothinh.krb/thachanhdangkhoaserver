﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ThachAnhDangKhoa.Models
{
    public class News
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? Id { get; set;}
        [Required]
        public string  Title { get; set;}
        [Required]
        public string HtmlContent { get; set; }
        public string SumaryContent { get; set; }
        [Required]
        public DateTime PostedDate { get; set; }
        [Required]
        public string Poster { get; set; }
    }
}
