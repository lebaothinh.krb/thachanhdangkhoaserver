﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ThachAnhDangKhoa.Models;

namespace ThachAnhDangKhoa.Models
{
    public class Topic
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TopicId { get; set; }
        [Required]
        public string TopicName { get; set; }
        public virtual ICollection<ProductTopic> ProductTopics { get; set; }
    }
}
