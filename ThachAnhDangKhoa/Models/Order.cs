﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ThachAnhDangKhoa.Models
{
    public enum OrderStatus
    {
        NOTSEEN,
        SEEN,
        HANDLING,
        FINISH
    }
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderId { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public OrderStatus Status { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<Product> Products { get; set; }

    }
}
