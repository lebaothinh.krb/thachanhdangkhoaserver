﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThachAnhDangKhoa.Models
{
    public class ProductTopic
    {
        public int ProductId { get; set; }
        public Product Product{ get; set; }
        public int TopicId { get; set; }
        public Topic Topic{ get; set; }
    }
}
