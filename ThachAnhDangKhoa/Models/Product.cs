﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ThachAnhDangKhoa.Models;

namespace ThachAnhDangKhoa.Models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? Id { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public string Material { get; set; }
        [Required]
        public string Size { get; set; }
        
        public string Imgs { get; set; }
        [Required]
        public string Price { get; set; }
        [NotMapped]
        public List<string> ImgsToList {
            get
            {
                if (Imgs == null) return null;
                return Imgs.Split(";").ToList<string>();
            }
            set
            {
                Imgs = String.Join(";", value);
            }
        }

        public virtual ICollection<ProductTopic> ProductTopics{ get; set; }

        public virtual TypeSearch Type { get; set; }

        public virtual Order Order { get; set; }
    }
}
