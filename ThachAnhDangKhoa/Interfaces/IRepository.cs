﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ThachAnhDangKhoa.Interfaces
{

    interface IRepository<TEntity> where TEntity: class
    {
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void Remove(TEntity entity);

        void Remove(int id);

        int Count();

        TEntity Get(int id);
        IEnumerable<TEntity> GetAll(List<string> includeString);
        IEnumerable<TEntity> GetByCondition(Expression<Func<TEntity, bool>> predicate, List<string> includeString);
    }
}
