﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ThachAnhDangKhoa.Models;

namespace ThachAnhDangKhoa.Models
{
    public class ThachAnhDangKhoaContext : DbContext
    {
        public ThachAnhDangKhoaContext (DbContextOptions<ThachAnhDangKhoaContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(p => new { p.Account, p.PhoneNumber })
                .IsUnique(true);
            modelBuilder.Entity<Topic>().HasIndex(p => new { p.TopicName }).IsUnique(true);
            modelBuilder.Entity<TypeSearch>().HasIndex(p => new { p.TypeName }).IsUnique(true);
            modelBuilder.Entity<Role>().HasIndex(p => new { p.RoleName }).IsUnique(true);
            modelBuilder.Entity<Product>().HasIndex(p => new { p.ProductName }).IsUnique(true);
            modelBuilder.Entity<Order>().HasMany(o => o.Products).WithOne(e => e.Order).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<ProductTopic>().HasKey(bc => new { bc.ProductId, bc.TopicId });
            modelBuilder.Entity<ProductTopic>()
                .HasOne(bc => bc.Product)
                .WithMany(b => b.ProductTopics)
                .HasForeignKey(bc => bc.ProductId);

            modelBuilder.Entity<ProductTopic>()
                .HasOne(bc => bc.Topic)
                .WithMany(c => c.ProductTopics)
                .HasForeignKey(bc => bc.TopicId);
        }

        public DbSet<ThachAnhDangKhoa.Models.Topic> Topic { get; set; }

        public DbSet<ThachAnhDangKhoa.Models.TypeSearch> Type { get; set; }
        
        public DbSet<ThachAnhDangKhoa.Models.Role> Role { get; set; }

        public DbSet<ThachAnhDangKhoa.Models.News> News { get; set; }

        public DbSet<ThachAnhDangKhoa.Models.Content> Content { get; set; }

        public DbSet<ThachAnhDangKhoa.Models.User> User { get; set; }

        public DbSet<ThachAnhDangKhoa.Models.Product> Product { get; set; }

        public DbSet<ThachAnhDangKhoa.Models.ProductTopic> ProductTopic { get; set; }

        public DbSet<ThachAnhDangKhoa.Models.Order> Order { get; set; }

        public DbSet<ThachAnhDangKhoa.Models.OrderByRequest> OrderByRequest { get; set; }


    }
}
