﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ThachAnhDangKhoa.Models;

namespace ThachAnhDangKhoa.Data
{
    public class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<ThachAnhDangKhoaContext>());
            }
        }

        private static int retryCount = 10;
        private static readonly TimeSpan delay = TimeSpan.FromSeconds(30);

        public static void SeedData(ThachAnhDangKhoaContext context)
        {
            int currentRetry = 0;

            for (; ; )
            {
                try
                {
                    // Call external service.

                    context.Database.Migrate();
                    #region --- SeedData ------

                    // Topic
                    var topics = new Topic[]{
                            new Topic() { TopicName = "Tất Cả" },
                            new Topic() { TopicName = "Ảnh Cưới" },
                            new Topic() { TopicName = "Công Giáo" },
                            new Topic() { TopicName = "Phật Giáo" },
                            new Topic() { TopicName = "Nghệ Thuật" },
                            new Topic() { TopicName = "Tây Nguyên" },
                            new Topic() { TopicName = "Người Nổi Tiếng" },
                            new Topic() { TopicName = "Thư Pháp" },
                            new Topic() { TopicName = "Thú Vật" }
                    };
                    // Type Search
                    var types = new TypeSearch[]
                    {
                        new TypeSearch() { TypeName = "Tất Cả" },
                        new TypeSearch() { TypeName = "Thạch Ảnh" },
                        new TypeSearch() { TypeName = "Mộc Ảnh" },
                        new TypeSearch() { TypeName = "Diệp Ảnh" }
                    };
                    // Role
                    var roles = new Role[]
                    {
                        new Role() { RoleName = "USER" },
                        new Role() { RoleName = "ADMIN" }
                    };
                    // News
                    var newes = new News[] {
                        new News() { Title = "Boxing icon has the will for a couple more fights", HtmlContent = "<p>The highly anticipated world championship fight will take place at 10am and is the second major boxing blockbuster in the nation after 43 years </ p >", PostedDate = DateTime.Now, Poster = "http://localhost:60699/Images/Newes/bg2.jpg", SumaryContent = "The highly anticipated world championship fight will take place at 10am and is the second major boxing blockbuster in the nation after 43 years" },
                        new News() { Title = "Boxing icon has the will for a couple more fights", HtmlContent = "<p>The highly anticipated world championship fight will take place at 10am and is the second major boxing blockbuster in the nation after 43 years </ p >", PostedDate = DateTime.Now, Poster = "http://localhost:60699/Images/Newes/bg2.jpg", SumaryContent = "The highly anticipated world championship fight will take place at 10am and is the second major boxing blockbuster in the nation after 43 years" }
                    };
                    // Contents
                    var contents = new Content[] {
                        new Content() { SumaryContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries", Img = "http://localhost:60699/Images/Contents/sample04.png", Topic = topics[1] },
                        new Content() { SumaryContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries", Img = "http://localhost:60699/Images/Contents/sample08.png", Topic = topics[7] },
                        new Content() { SumaryContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries", Img = "http://localhost:60699/Images/Contents/sample13.png", Topic = topics[6] },
                        new Content() { SumaryContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries", Img = "http://localhost:60699/Images/Contents/sample06.png", Topic = topics[2] },
                        new Content() { SumaryContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries", Img = "http://localhost:60699/Images/Contents/sample18.png", Topic = topics[3] },
                        new Content() { SumaryContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries", Img = "http://localhost:60699/Images/Contents/sample01.png", Topic = topics[5] },
                        new Content() { SumaryContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries", Img = "http://localhost:60699/Images/Contents/sample03.png", Topic = topics[4] }
                    };
                    // Users
                    var users = new User[] {
                        new User() { UserName = "Lê Đăng Khoa", Address = "Đà Nẵng", Email = "ledangkhoa.krb@gmail.com", PhoneNumber = "0354141413", Account = "admin", Password = "admin", Role = roles[1] },
                        new User() { UserName = "Lê Bảo Thịnh", Address = "Hồ Chí Minh", Email = "lebaothinh.krb@gmail.com", PhoneNumber = "0347013955", Account = "lebaothinh", Password = "thinh231148", Role = roles[0] }
                    };
                    // Product
                    var products = new Product[] {
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 1", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 2", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 3", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 4", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 5", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 6", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 7", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 8", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 11", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 12", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 13", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 14", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 15", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 16", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 17", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 18", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 21", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 22", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 23", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 24", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 25", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 26", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 27", Type = types[1], Price = "Liên Hệ" },
                        new Product() { Imgs = "http://localhost:60699/Images/Products/sample04.jpg;http://localhost:60699/Images/Products/sample01.jpg;http://localhost:60699/Images/Products/sample03.jpg;http://localhost:60699/Images/Products/sample06.jpg", Material = "Đá Granic", Size = "20x30cm", ProductName = "Lorem Ipsum is simply 28", Type = types[1], Price = "Liên Hệ" },
                    };
                    //Product Topics
                    var productTopics = new ProductTopic[]
                    {
                        new ProductTopic(){ Product = products[0], Topic = topics[1] },
                        new ProductTopic(){ Product = products[1], Topic = topics[1] },
                        new ProductTopic(){ Product = products[2], Topic = topics[1] },
                        new ProductTopic(){ Product = products[3], Topic = topics[1] },
                        new ProductTopic(){ Product = products[4], Topic = topics[1] },
                        new ProductTopic(){ Product = products[5], Topic = topics[1] },
                        new ProductTopic(){ Product = products[6], Topic = topics[1] },
                        new ProductTopic(){ Product = products[7], Topic = topics[1] },
                    };
                    // Orders
                    var orders = new Order[] {
                        new Order() { OrderDate = DateTime.Now, User = users[1], Status = OrderStatus.NOTSEEN, Products = new List<Product>{ products[0], products[1]} }
                    };
                    // OrderByRequests
                    var orderByRequests = new OrderByRequest[] {
                        new OrderByRequest() { OrderDate = DateTime.Now, User = users[1], Status = OrderStatus.NOTSEEN, DescribeProduct = "<p>Tôi muốn làm một viên đá như hình này</p>" }
                    };
                    #endregion
                    if (!context.Topic.Any())
                    {
                        context.Topic.AddRange(topics);
                        context.SaveChanges();
                    }
                    if (!context.Type.Any())
                    {
                        context.Type.AddRange(types);
                        context.SaveChanges();
                    }
                    if (!context.Role.Any())
                    {
                        context.Role.AddRange(roles);
                        context.SaveChanges();
                    }
                    if (!context.News.Any())
                    {
                        context.News.AddRange(newes);
                        context.SaveChanges();
                    }
                    if (!context.Content.Any())
                    {
                        context.Content.AddRange(contents);
                        context.SaveChanges();
                    }
                    if (!context.User.Any())
                    {
                        context.User.AddRange(users);
                        context.SaveChanges();
                    }
                    if (!context.Product.Any())
                    {
                        context.Product.AddRange(products);
                        context.SaveChanges();
                    }
                    if (!context.ProductTopic.Any())
                    {
                        context.ProductTopic.AddRange(productTopics);
                        context.SaveChanges();
                    }
                    if (!context.Order.Any())
                    {
                        context.Order.AddRange(orders);
                        context.SaveChanges();
                    }
                    if (!context.OrderByRequest.Any())
                    {
                        context.OrderByRequest.AddRange(orderByRequests);
                        context.SaveChanges();
                    }
                    context.SaveChanges();
                    break;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Operation Exception");

                    currentRetry++;

                    // Check if the exception thrown was a transient exception
                    // based on the logic in the error detection strategy.
                    // Determine whether to retry the operation, as well as how
                    // long to wait, based on the retry strategy.
                    if (currentRetry > retryCount)
                    {
                        // If this isn't a transient error or we shouldn't retry,
                        // rethrow the exception.
                        throw ex;
                    }

                }

                // Wait to retry the operation.
                // Consider calculating an exponential delay here and
                // using a strategy best suited for the operation and fault.
                Thread.Sleep(delay);
            }

            //var strategy = context.Database.CreateExecutionStrategy();
            //strategy.Execute( () =>
            //{
            //    // Achieving atomicity between original Catalog database operation and the
            //    // IntegrationEventLog thanks to a local transaction
            //    using (var transaction = context.Database.BeginTransaction())
            //    {
            //        context.Database.Migrate();
            //        transaction.Commit();
            //    }
            //});

        }
    }
}
