﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThachAnhDangKhoa.ViewModels
{
    public class UploadViewModel
    {
        public IFormFile file { get; set; }
        public string fileName { get; set; }
    }
}
