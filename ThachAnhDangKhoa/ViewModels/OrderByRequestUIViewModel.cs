﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ThachAnhDangKhoa.Models;

namespace ThachAnhDangKhoa.ViewModels
{
    public class OrderByRequestUIViewModel
    {
        public int id;
        public string customerName;
        public string email;
        public string phoneNumber;
        public string address;
        public string describeProduct;
        public DateTime orderDate;
        public OrderStatus status;
    }
}
